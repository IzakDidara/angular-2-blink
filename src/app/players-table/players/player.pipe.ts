import {Pipe, PipeTransform} from '@angular/core';
import {Player} from '../../shared/player.model';

@Pipe({
    name: 'playerFilter'
})
export class PlayerPipe implements PipeTransform {
    transform(players: Player[], table: number): any {
        return players.filter(player => player.table === table);
    }
}