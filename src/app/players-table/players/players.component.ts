import {Component, Input, OnInit} from '@angular/core';
import {PlayerService} from '../../shared/player.service';
import {Player} from '../../shared/player.model';


@Component({
    selector: 'app-players',
    templateUrl: './players.component.html',
    styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {
    @Input() table: number;
    players: Player[];

    constructor(private playerService: PlayerService) {
    }

    ngOnInit() {
        this.players = this.playerService.getPlayers();
        this.playerService.playersChanged.subscribe((players: Player[]) => {
            this.players = players;
        });
    }
}