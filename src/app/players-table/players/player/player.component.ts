import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../../shared/player.model';
import {PlayerService} from '../../../shared/player.service';

@Component({
    selector: 'app-player',
    templateUrl: './player.component.html',
    styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
    @Input() player: Player;
    selectedPlayer: Player;

    constructor(private playerService: PlayerService) {
        this.selectedPlayer = this.playerService.getSelectedPlayer();
    }

    ngOnInit() {
        this.playerService.playerSelectedChanged.subscribe((player: Player) => {
                this.selectedPlayer = player;
            }
        );
    }

    onSelected() {
        this.playerService.playerSelectedChanged.emit(this.player);
        this.playerService.setSelectedPlayer(this.player);
    }
}