import {Component} from '@angular/core';
import {Player} from '../../shared/player.model';
import {PlayerService} from '../../shared/player.service';

@Component({
    selector: 'app-nav-buttons',
    templateUrl: './nav-buttons.component.html',
    styleUrls: ['./nav-buttons.component.scss']
})
export class NavButtonsComponent {
    selectedPlayer: Player;

    constructor(private playerService: PlayerService) {
        this.playerService.playerSelectedChanged.subscribe((player: Player) => {
                this.selectedPlayer = player;
            }
        );
    }

    onChangeTable() {
        const newPlayer = this.selectedPlayer;
        if (newPlayer.table === 1) {
            newPlayer.table = 2;
        } else {
            newPlayer.table = 1;
        }

        this.playerService.updatePlayer(newPlayer);
    }
}