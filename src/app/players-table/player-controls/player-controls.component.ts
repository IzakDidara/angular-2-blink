import {Component} from '@angular/core';
import {Player} from '../../shared/player.model';
import {PlayerService} from '../../shared/player.service';
import {positions} from './positions';
import {FormGroup} from '@angular/forms';

@Component({
    selector: 'app-player-controls',
    templateUrl: './player-controls.component.html',
    styleUrls: ['./player-controls.component.scss']
})
export class PlayerControlsComponent {
    selectedValue = positions[0].position;
    selectedPlayer: Player;
    popupIsOpened: boolean = false;
    positions = positions;
    playerName: string = '';
    form: FormGroup

    constructor(private playerService: PlayerService) {
        this.playerService.playerSelectedChanged.subscribe((player: Player) => {
            this.selectedPlayer = player;
        });
    }

    onDeletePlayer() {
        this.playerService.deletePlayer(this.selectedPlayer);
    }

    togglePopupOpened(popupState: boolean) {
        this.popupIsOpened = popupState;
    }

    addNewPlayer(playerForm: any) {
        this.playerService.addPlayer(this.selectedValue, this.playerName);
        this.popupIsOpened = false;
        this.selectedValue = positions[0].position;
        this.playerName = '';
        playerForm.form.markAsPristine();
        playerForm.form.markAsUntouched();
    }
}