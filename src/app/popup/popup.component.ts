import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'app-popup',
    templateUrl: './popup.component.html',
    styleUrls: ['./popup.component.scss']
})
export class PopupComponent {
    @Input() opened: boolean = false;
    @Input() name: string = 'Name of popup';
    @Output() onTogglePopup: EventEmitter<any> = new EventEmitter();

    onCloseClick() {
        this.onTogglePopup.emit(false);
        this.opened = false;
    }
}