import {Player} from './player.model';
import {EventEmitter} from '@angular/core';

export class PlayerService {
    playerSelectedChanged = new EventEmitter<Player>();
    playersChanged = new EventEmitter<Player[]>();

    private selectedPlayer: Player;
    private players: Player[] = [
        new Player('Ronaldo', 'ST', 1),
        new Player('Pletikosa', 'GK', 1),
        new Player('Robert Jarni', 'LW', 1),
        new Player('Dado Pršo', 'ST', 1),
        new Player('Darijo Srna', 'RW', 1),
        new Player('Asanović', 'CB', 1),
        new Player('Modrić', 'DM', 1)
    ];

    getPlayers() {
        return this.players.slice();
    }

    getSelectedPlayer() {
        return this.selectedPlayer;
    }

    setSelectedPlayer(player: Player) {
        this.selectedPlayer = Object.assign({}, player);
    }

    updatePlayer(player: Player) {
        this.players = this.players.map(p => {
            if (p.id === player.id) {
                return player;
            }
            return p;
        });
        this.selectedPlayer = player;
        this.playersChanged.emit(this.players);
    }

    deletePlayer(player: Player) {
        this.players = this.players.filter(p => {
            return p.id !== player.id;
        });
        this.selectedPlayer = undefined;
        this.playerSelectedChanged.emit(this.selectedPlayer);
        this.playersChanged.emit(this.players);
    }

    addPlayer(selectedValue: string, playerName: string) {
        const newPlayer = new Player(playerName, selectedValue, 1);
        this.players = this.players.concat([newPlayer]);
        this.playersChanged.emit(this.players);
    }
}
