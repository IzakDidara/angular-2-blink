export class Player {
    public id : string;
    public name: string;
    public position: string;
    public table: number;

    constructor(name: string, position: string, table: number) {
        this.id = Math.random().toString(16).slice(2);
        this.name = name;
        this.position = position;
        this.table = table;
    }
}