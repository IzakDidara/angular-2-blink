import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {PlayersTableComponent} from './players-table/players-table.component';
import {NavButtonsComponent} from './players-table/nav-buttons/nav-buttons.component';
import {PlayersComponent} from './players-table/players/players.component';
import {PlayerComponent} from './players-table/players/player/player.component';
import {PlayerControlsComponent} from './players-table/player-controls/player-controls.component';
import {PopupComponent} from './popup/popup.component';

import {PlayerService} from './shared/player.service';

import {PlayerPipe} from './shared/player.pipe';


@NgModule({
    declarations: [
        AppComponent,
        PlayersTableComponent,
        NavButtonsComponent,
        PlayersComponent,
        PlayerComponent,
        PlayerControlsComponent,
        PopupComponent,
        PlayerPipe
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule
    ],
    providers:[PlayerService],
    bootstrap: [AppComponent]
})
export class AppModule {
}